"""Chip-8 emulator/language written in Python3""" 

#4095 bytes of memory
#Interpreter area: 0x000 to 0x1FF
ram = [0xFFF for i in range(0xFFF)]

#16 general-purpose 8-bit registers
V0 = 0x00 
V1 = 0x00
V2 = 0x00
V3 = 0x00
V4 = 0x00
V5 = 0x00
V6 = 0x00
V7 = 0x00
V8 = 0x00
V9 = 0x00
VA = 0x00
VB = 0x00
VC = 0x00
VD = 0x00
VE = 0x00
VF = 0x00

#16-bit register used to store memory addresses, thus usually only lowest (rightmost) 12 bits are used
I = 0x0000

#Special purpose 16-bit registers for delay and sound timers. When registers are non-zero, they are automatically decremented at 60Hz
delay_register = 0x00
sound_register = 0x00

#pseudo-registers
pc = 0x0000 #Program Counter: used to store currently executing address
sp = 0x00 #Stack Pointer: used to point to the topmost level of the stack

#Stack: an array of 16 16-bit values, used to store the address that the interpreter should return to when finished with a subroutine. Chip-8 allows for up to 16 levels of nested subroutines.
stack = [0xFFFF for i in range(0xF)]

"""
Keyboard layout:

|1|2|3|C|
|4|5|6|D|
|7|8|9|E|
|A|0|B|F|

Display:
64x32-pixel monochrome display with this format:
(0, 0)    (63, 0)
(0, 31)   (63, 31) 
"""

#Character sprites; stored in interpreter area of Chip-8 memory (0x000 to 0x1FF)
sprite_0 = [
    [0b11110000], #0xF0
    [0b10010000], #0x90
    [0b10010000], #0x90
    [0b10010000], #0x90
    [0b11110000]  #0xF0
]

sprite_1 = [
    [0b00100000], #0x20
    [0b01100000], #0x60
    [0b00100000], #0x20
    [0b00100000], #0x20
    [0b01110000]  #0x70
]

sprite_2 = [
    [0b11110000], #0xF0
    [0b00010000], #0x10
    [0b11110000], #0xF0
    [0b10000000], #0x80
    [0b11110000]  #0xF0
]

sprite_3 = [
    [0b11110000], #0xF0
    [0b00010000], #0x10
    [0b11110000], #0xF0
    [0b00010000], #0x10
    [0b11110000]  #0xF0
]

sprite_4 = [
    [0b10010000], #0x90
    [0b10010000], #0x90
    [0b11110000], #0xF0
    [0b00010000], #0x10
    [0b00010000]  #0x10
]

sprite_5 = [
    [0b11110000], #0xF0
    [0b10000000], #0x80
    [0b11110000], #0xF0
    [0b00010000], #0x10
    [0b11110000]  #0xF0
]

sprite_6 = [
    [0b11110000], #0xF0
    [0b10000000], #0x80
    [0b11110000], #0xF0
    [0b10010000], #0x90
    [0b11110000]  #0xF0
]

sprite_7 = [
    [0b11110000], #0xF0
    [0b00010000], #0x10
    [0b00100000], #0x20
    [0b01000000], #0x40
    [0b01000000]  #0x40
]

sprite_8 = [
    [0b11110000], #0xF0
    [0b10010000], #0x90
    [0b11110000], #0xF0
    [0b10010000], #0x90
    [0b11110000]  #0xF0
]

sprite_9 = [
    [0b11110000], #0xF0
    [0b10010000], #0x90
    [0b11110000], #0xF0
    [0b00010000], #0x10
    [0b11110000]  #0xF0
]

sprite_A = [
    [0b11110000], #0xF0
    [0b10010000], #0x90
    [0b11110000], #0xF0
    [0b10010000], #0x90
    [0b10010000]  #0x90
]

sprite_B = [
    [0b11100000], #0xE0
    [0b10010000], #0x90
    [0b11100000], #0xE0
    [0b10010000], #0x90
    [0b11100000]  #0xE0
]

sprite_C = [
    [0b11110000], #0xF0
    [0b10000000], #0x80
    [0b10000000], #0x80
    [0b10000000], #0x80
    [0b11110000]  #0xF0
]

sprite_D = [
    [0b11100000], #0xE0
    [0b10010000], #0x90
    [0b10010000], #0x90
    [0b10010000], #0x90
    [0b11100000]  #0xE0
]

sprite_E = [
    [0b11110000], #0xF0
    [0b10000000], #0x80
    [0b11110000], #0xF0
    [0b10000000], #0x80
    [0b11110000]  #0xF0
]

sprite_F = [
    [0b11110000], #0xF0
    [0b10000000], #0x80
    [0b11110000], #0xF0
    [0b10000000], #0x80
    [0b10000000]  #0x80
]

#Put sprites into memory
chars = [sprite_0, sprite_1, sprite_2, sprite_3, sprite_4, sprite_5, sprite_6, sprite_7, sprite_8, sprite_9, sprite_A, sprite_B, sprite_C, sprite_D, sprite_E, sprite_F]

for i in range(16):
    ram[i] = chars[i]

#Standard Chip-8 Instructions

def SYS(addr):
    